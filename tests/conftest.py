#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
import unittest.mock

import pytest
from click.testing import CliRunner

from jenkins_lockable_resources.main import cli as cli_entry
from jenkins_lockable_resources.main import AppFactory
from jenkins_lockable_resources.app import LockableResourceApp
from jenkins_lockable_resources.model import LockableResources

FAKE_JENKINS_URL = "http://fake.jenkins.com"

RESOURCES_DATA = {
    "resources": [
        {"name": "skiny.domain.com", "state": "FREE", "owner": None, "label": "label1"},
        {"name": "bobby.domain.com", "state": "FREE", "owner": None, "label": "label2"},
        {
            "name": "fluffy.domain.com",
            "state": "RESERVED",
            "owner": "McGiver",
            "label": "label1",
        },
        {
            "name": "fatty.domain.com",
            "state": "LOCKED",
            "owner": "KnightOfNightly",
            "label": "label2",
        },
    ]
}

RESOURCES = RESOURCES_DATA["resources"]
RESERVED_RESOURCES = [r for r in RESOURCES if r["state"] == "RESERVED"]
FREE_RESOURCES = [r for r in RESOURCES if r["state"] == "FREE"]


# pylint: disable=redefined-outer-name


@pytest.fixture
def jenkins(mocker):
    mock = mocker.MagicMock()
    mock.baseurl = FAKE_JENKINS_URL
    mock.username = "McGiver"
    return mock


@pytest.fixture
def streamer(mocker):
    return mocker.patch("jenkins_lockable_resources.main.ClickStreamer")


@pytest.fixture
def lockable_resources(monkeypatch, jenkins):
    def fake_poll(*args, **kw):
        return RESOURCES_DATA

    monkeypatch.setattr(LockableResources, "_poll", fake_poll)

    # Disable cache for testing
    return LockableResources(jenkins)


@pytest.fixture
def app(streamer, lockable_resources):
    return LockableResourceApp(streamer, lockable_resources)


pytestmark = [pytest.mark.click_cli_must_fail, pytest.mark.click_cli_skip_exit_code]


@pytest.fixture
def app_fact_mock(monkeypatch, mocker):
    ctor = mocker.MagicMock()

    def create_app(factory):
        return ctor(*factory._args, **factory._kwargs)

    monkeypatch.setattr(AppFactory, "create_app", create_app)
    return ctor


@pytest.fixture
def app_mock(monkeypatch, mocker):
    return mocker.MagicMock()


@pytest.fixture
def cli(request, mocker, monkeypatch, app_mock):
    runner = CliRunner()
    markers = [m.name for m in request.node.own_markers]

    def from_testing(*args, **kwargs):
        return app_mock

    monkeypatch.setattr(LockableResourceApp, "from_default", from_testing)

    def cli_wrapper(*args, **kwargs):
        result = runner.invoke(cli_entry, args, **kwargs)
        if "cli_xfail" in markers:
            assert result.exit_code != 0, "CLI expected to return non null exit code"
        elif "cli_ifail" not in markers:
            assert result.exit_code == 0, "CLI expected to return null exit code"
        return result

    return cli_wrapper


def _param_fixture(request):
    return request.param


resource = pytest.fixture(params=RESOURCES, ids=lambda r: r["name"])(_param_fixture)
reserved = pytest.fixture(params=RESERVED_RESOURCES, ids=lambda r: r["name"])(
    _param_fixture
)
free = pytest.fixture(params=FREE_RESOURCES, ids=lambda r: r["name"])(_param_fixture)
