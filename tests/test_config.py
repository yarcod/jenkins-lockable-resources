import os

import pytest

from jenkins_lockable_resources.config import ConfigLoader, Config


def test_config_update_type_error():
    with pytest.raises(TypeError):
        Config().update("INVALID")


CONFIG_1 = {
    "opt_string": "string",
    "opt_tuple": ("value1.1", "value1.2"),
    "opt_dict": {"opt_string": "string", "opt_tuple": ("value1.1", "value1.2")},
    "opt_type_mismatch": "string",
}
CONFIG_2 = {
    "opt_string": "string new",
    "opt_tuple": ("value2.1", "value2.2", "value2.3"),
    "opt_dict": {
        "opt_string": "string",
        "opt_tuple": ("value2.1", "value2.2", "value2.3"),
    },
    "opt_type_mismatch": {"opt_test": "value"},
    "opt_new": "Other string",
}
CONFIG_MERGED = {
    "opt_string": "string new",
    "opt_tuple": ("value2.1", "value2.2", "value2.3"),
    "opt_dict": {
        "opt_string": "string",
        "opt_tuple": ("value2.1", "value2.2", "value2.3"),
    },
    "opt_type_mismatch": {"opt_test": "value"},
    "opt_new": "Other string",
}
CONFIG_MERGED_MISSMATCH_IGNORED = {
    "opt_string": "string new",
    "opt_tuple": ("value2.1", "value2.2", "value2.3"),
    "opt_dict": {
        "opt_string": "string",
        "opt_tuple": ("value2.1", "value2.2", "value2.3"),
    },
    "opt_type_mismatch": "string",
    "opt_new": "Other string",
}


def test_config_update_merge():
    conf = Config()
    conf.update(CONFIG_1)
    conf.update(CONFIG_2)
    assert conf == CONFIG_MERGED


def test_config_update_merge_strict():
    conf = Config(policy="strict")
    conf.update(CONFIG_1)
    with pytest.raises(TypeError):
        conf.update(CONFIG_2)


def test_config_update_merge_ignore_type_missmatch():
    conf = Config(policy="ignore")
    conf.update(CONFIG_1)
    conf.update(CONFIG_2)
    assert conf == CONFIG_MERGED_MISSMATCH_IGNORED


def test_config_loader():
    loader = ConfigLoader(
        [__file__.replace(".py", ".yml"), __file__.replace(".py", "_extra.yml")]
    )
    conf = loader.load()
    assert conf["owned"]["user"] == "MrBean"
