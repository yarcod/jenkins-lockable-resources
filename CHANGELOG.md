# 1.0.0

**Changes**

* Added options for matching label and state
* Added is free API to model
* Extended testing to extend code coverage

# 0.1.2

**Fixed**

* Getting resources length from LockableResources object

**Changes**

* Print an error instead of warning when no resources matches filter
* Extended testing to extend code coverage
* Support for python 3.6 to 3.8 and removed support of python <= 3.5
* Added usage examples in README file
    
# 0.1.1

Initial release

**Features**
* Resource library APIs
    * List
    * Reserve
    * Unreserve
* CLI
    * List
    * Reserve
    * Unreserve
    * Find owned resource